# -*- coding: utf-8 -*-


#%% Import all relevant libraries here
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import os

import wfdb
from wfdb import processing


#%% Data directory
# assign the correct path to downloaded data and filenames
# realpath is this script path, while data is subfolder in the same dir
# parent_dir =  os.path.dirname(os.path.realpath(__file__))
parent_dir = os.path.abspath('')
data_dir =  os.path.join(parent_dir,'data')


filenames = []

for i in range(10):
    filenames.append('infant'+str(i+1))

# print and list all relevant filenames, as a quick check
print(filenames)

#%% EDA

# loop through all files and save the dict

dict_ecg = []
dict_resp = []

def summarize(postfix):
    for i in range(0,10):
        path = data_dir+'/'+filenames[i]+postfix
        df = pd.DataFrame([wfdb.rdrecord(path).__dict__])
        readAndAppend(data_dir+'\data{postfix}.csv'.format(postfix=postfix), df)      


def readAndAppend(filepath, df):
    # if file exists, read from file    
    if(os.path.isfile(filepath)): 
        old_df = pd.read_csv(filepath)
    else:
        old_df = pd.DataFrame()
    df = pd.concat([old_df,df])
    df.to_csv(filepath, index=False)

summarize('_ecg')
summarize('_resp')

#%% 
# read from saved csv files
  
df_ecg = pd.read_csv(data_dir+'\data_ecg.csv', index_col=0)    
df_resp = pd.read_csv(data_dir+'\data_resp.csv', index_col=0)

#%% ALT


# Load the WFDB record and the physical samples
ecg_record = wfdb.rdrecord(f"{data_dir}/infant1_ecg", sampfrom=0, sampto=10000, channels=[0])


sig = ecg_record.p_signal
fs = ecg_record.fs

# Use the GQRS algorithm to detect QRS locations in the first channel
qrs_inds = processing.qrs.gqrs_detect(sig=ecg_record.p_signal[:,0], fs=fs)

# compute hr
hrs = processing.hr.compute_hr(sig_len=sig.shape[0], qrs_inds=qrs_inds, fs=fs)

test = pd.DataFrame(hrs).describe()
